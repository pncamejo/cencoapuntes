import React from 'react'
import ReactDOM from 'react-dom/client'
import {CencoApuntes} from './CencoApuntes'
import './index.css'
import {BrowserRouter} from 'react-router-dom'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
      <CencoApuntes />
    </BrowserRouter>
  </React.StrictMode>,
)
