import { useState } from 'react'
import { Routes, Route, useNavigate, Navigate } from 'react-router-dom'
import NavBar from './cencoapuntes/components/NavBar'
import { DockerKubernetes, Error404, Home, ComandLine, GitMastery, WebDevelopment, Nest, React } from './cencoapuntes/pages'
import { PrivateRoutes, PublicRoutes} from './router/'


export function CencoApuntes() {
  const [login, setLogin] = useState(false)

  return (
    <>
      <NavBar setLogin={setLogin} login={login} />
      <Routes>
        {login ? 
        <>
          <Route path='/home' element={<Navigate to='/admin'/>}/>
          <Route path='/admin/*' element={<PrivateRoutes/>} />
          <Route path='/*' element={<Navigate to='/admin'/>}/>

        </>
        :
        <>
          <Route path='/admin/*'  element={<Navigate to='/home' />} />
          <Route path='/*' element={<PublicRoutes/>} />
        </>
        }

        <Route path="/home" element={<Home/>} />
        <Route path="/comand_line" element={<ComandLine/>} />
        <Route path="/comand_line/:post" element={<ComandLine/>} />
        <Route path="/web_development" element={<WebDevelopment/>} />
        <Route path="/web_development/:post" element={<WebDevelopment/>} />
        <Route path="/git" element={<GitMastery/>} />
        <Route path="/git/:post" element={<GitMastery/>} />
        <Route path="/nest" element={<Nest/>} />
        <Route path="/react" element={<React/>} />
        <Route path="/react/:post" element={<React/>} />
        <Route path="/docker_kubernetes" element={<DockerKubernetes/>} />
        <Route path="/docker_kubernetes/:post" element={<DockerKubernetes/>} />
      </Routes>

    </>
  )
}

