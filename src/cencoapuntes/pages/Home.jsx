import { Link } from "react-router-dom";

export  function Home() {
  return (
    <div>
      <div>
        <details>
          <summary>
            <Link to='/web_development'>Web Develpopment Concepts</Link>
          </summary>
          <ul>
            <li><Link to='/web_development/aaa'>aaaa</Link></li>
            <li><Link to='/web_development/bbb'>bbb</Link></li>
            <li><Link to='/web_development/ccc'>ccc</Link></li>
          </ul>
        </details>
        <details>
          <summary>
            <Link to='/git'>Git Mastery: GitLab & GitHub</Link>
          </summary>
          <ul>
            <li><Link to='/git/aaa'>aaaa</Link></li>
            <li><Link to='/git/bbb'>bbb</Link></li>
            <li><Link to='/git/ccc'>ccc</Link></li>
          </ul>
        </details>
      </div>

      <div>
        <details>
          <summary>
            <Link to='/comand_line'>Comand Line Linux</Link>
          </summary>
          <ul>
            <li><Link to='/comand_line/aaa'>aaaa</Link></li>
            <li><Link to='/comand_line/bbb'>bbb</Link></li>
            <li><Link to='/comand_line/ccc'>ccc</Link></li>
          </ul>
        </details>
        <input type="search" name="" id="" />
        <details>
          <summary>
            <Link to='/nest'>Nest</Link>
          </summary>
          <ul>
            <li><Link to='/nest/aaa'>aaaa</Link></li>
            <li><Link to='/nest/bbb'>bbb</Link></li>
            <li><Link to='/nest/ccc'>ccc</Link></li>
          </ul>
        </details>
      </div>
      <div>
        <details>
          <summary>
            <Link to='/react'>React</Link>
          </summary>
          <ul>
            <li><Link to='/react/aaa'>aaaa</Link></li>
            <li><Link to='/react/bbb'>bbb</Link></li>
            <li><Link to='/react/ccc'>ccc</Link></li>
          </ul>
        </details>
        <details>
          <summary>
            <Link to='/docker_kubernetes'>Docker & Kubernetes</Link>
          </summary>
          <ul>
            <li><Link to='/docker_kubernetes/aaa'>aaaa</Link></li>
            <li><Link to='/docker_kubernetes/bbb'>bbb</Link></li>
            <li><Link to='/docker_kubernetes/ccc'>ccc</Link></li>
          </ul>
        </details>
      </div>
    </div>
  )
}
