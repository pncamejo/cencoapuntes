
export default function NavBar({setLogin, login}) {
  return (
    <>
        <button onClick={() => setLogin(login => !login)}>{
            login ? 'Logout' : 'Login'
        }</button>
    </>
  )
}
