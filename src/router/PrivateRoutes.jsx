import { Routes, Route } from "react-router-dom"
import { AdminProfile } from "../cencoapuntes/pages/adminPages/AdminProfile"
import { DockerKubernetes } from "../cencoapuntes/pages/adminPages/DockerKubernetes"

export function PrivateRoutes() {
  return (
    <Routes>
        <Route path="*" element={<AdminProfile/>} />
        <Route path="/docker_kubernetes" element={<DockerKubernetes/>} />

    </Routes>
  )
}
