import { Routes, Route, Navigate } from "react-router-dom"
import { DockerKubernetes, Home } from "../cencoapuntes/pages"
import Login from "../cencoapuntes/pages/Login"

export function PublicRoutes() {
  return (
    <Routes>
        <Route path="/login" element={<Login/>} />
        <Route path="*" element={<Navigate to='home'/>}/>
    </Routes>
  )
}